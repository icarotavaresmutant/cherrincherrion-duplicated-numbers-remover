module.exports = {
    BASE_URL: 'http://connector.brazilsouth.cloudapp.azure.com:8080',
    PATHS: {
        BALANCES: '/balances',
        ACCUMULATED_BALANCES: '/accumulatedBalances',
        PRODUCTS: '/products',
        BUCKETS: '/buckets',
        PAYMENTS: '/payments',
        INVOICES: '/invoices',
    },
    API_INDEX_NAME: [
        'accBalances',
        'balances',
        'buckets',
        'invoices',
        'payments',
        'products',
    ],
};

const {
    getPhoneDatasetFromCherrinCherrionFiles,
    getRepeatedPhones,
    generateRandomPhonesFromDataset,
} = require('./services/RandomFileNumberGenerator');

const {
    getDatasetFromRandomPhoneFiles,
} = require('./utils/file');

const {
    replacePhoneOnCherrinCherrionTestFiles,
} = require('./services/CherrinCherrionFilesUpdater');

const {
    mongoUpdater,
} = require('./services/MongoUpdater');

// 11.07.09.js
//  massa antiga -11996148616

// Phase 1 - Generate random_phones
// const phoneDataset = getPhoneDatasetFromCherrinCherrionFiles();
// const repeatedPhoneDataset = getRepeatedPhones(phoneDataset);
// console.log(repeatedPhoneDataset.length);
// generateRandomPhonesFromDataset(repeatedPhoneDataset);

const randomPhoneFilesDataset = getDatasetFromRandomPhoneFiles();
// Phase 2 - Replace in cherrin cherrion
// replacePhoneOnCherrinCherrionTestFiles(randomPhoneFilesDataset);

// Phase 3 - Add to mongodb
mongoUpdater(randomPhoneFilesDataset);
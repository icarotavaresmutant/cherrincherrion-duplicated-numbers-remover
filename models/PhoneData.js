class PhoneData {
    /**
     * @param {String | Number} phoneNumber
     * @param {Array} fileList
     */
    constructor (phoneNumber, fileList = []) {
        this.phoneNumber = phoneNumber;
        this.fileList = fileList;
    }

    get DDD () {
        return String(this.phoneNumber).slice(0,2);
    }

    get phoneNumberWithoutDDD () {
        return String(this.phoneNumber).slice(2);
    }
}

module.exports = PhoneData;

const {
    readFile,
    writeFile,
} = require('../utils/file');

const replacePhoneOnCherrinCherrionTestFiles = (dataset) => {
    dataset.forEach((data, index) => {
        const cherrinCherrionTestFile = readFile(data.file);
        const newFileData = cherrinCherrionTestFile.replace(data.originalPhoneNumber, data.newPhoneNumber);
        writeFile(data.file, newFileData);
        console.log(`${index} [File]=${data.file}; [old]=${data.originalPhoneNumber}; [new]=${data.newPhoneNumber}`)
    });
}

module.exports = {
    replacePhoneOnCherrinCherrionTestFiles,
};

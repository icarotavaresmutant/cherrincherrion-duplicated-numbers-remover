const { getPostMethod } = require('./SentinelManagerAPIConnection');

const mongoUpdater = async (dataset) => {
    for(let i = 0; i < dataset.length; i++) {
        const data = dataset[i];
        const apisResultEntries = Object.entries(data.apisResult);
        for (let j = 0; j < apisResultEntries.length; j++) {
            const [postMethod, postData] = apisResultEntries[j];
            const postResult = await getPostMethod(postMethod)(data.newPhoneNumber, postData);
            console.log(postResult);
            console.log(`[File]=${data.file}; [PostMethod]=${postMethod}; [Number]=${data.newPhoneNumber}`);
        }
    }
}

module.exports = {
    mongoUpdater,
};

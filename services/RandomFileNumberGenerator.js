const {
    readFile,
    writeRandomPhoneFile,
    getCherrinCherrionFiles,
    mountRandomPhoneNumberJSONFile,
} = require('../utils/file');
const PhoneData = require('../models/PhoneData');
const { generateRandomPhoneNumberWithSpecificDDD } = require('../utils/phoneNumber');
const { getAPIsData } = require('./SentinelManagerAPIConnection');

/**
 * Return a dataset (array with PhoneDatas) with the read of
 * all CherrinCherrion test files. (files inside __tests__ folder)
 * @returns {PhoneData[]}
 */
const getPhoneDatasetFromCherrinCherrionFiles = () => getCherrinCherrionFiles()
    .reduce((acc, file) => {
        const fileRead = readFile(file);
        const phoneNumberRegex = /const\smsisdn\s=\s('|`|")\d{11}/gi;
        const onlyPhoneNumberRegex = /\d{11}/;
        const completeRegexResult = fileRead.match(phoneNumberRegex);
        const regexResult = completeRegexResult && completeRegexResult[0].match(onlyPhoneNumberRegex);
        const testPhoneNumber = regexResult && regexResult[0];
        if (!testPhoneNumber) return acc;
        if (!acc[testPhoneNumber]) {
            acc[testPhoneNumber] = new PhoneData(testPhoneNumber);
        }
        acc[testPhoneNumber].fileList.push(file);
        return acc;
    }, {});

/**
 * Get only phones that appears more than once.
 * @param {PhoneData[]} phoneDataset
 * @returns {PhoneData[]}
 */
const getRepeatedPhones = (phoneDataset) => Object.values(phoneDataset)
    .filter(phoneData => phoneData.fileList.length > 1);

/**
 * Check if phone exists on our mock API.
 * @param {String} phoneNumber
 * @throws Will throw an error if some request throws an error.
 * @returns {Boolean} Return the condition if the phone exists.
 */
const checkIfPhoneExists = async (phoneNumber) => {
    try {
        const results = await getAPIsData(phoneNumber);
        return !results.every(({ code }) => code === 404);
    } catch (err) {
        throw err;
    }
}

/**
 * @param {PhoneData} phoneData
 * @returns {Number} A random phone number that doesn't exists on our mock API.
 */
const generateRandomPhone = async (phoneData) => {
    let randomPhoneNumber = generateRandomPhoneNumberWithSpecificDDD(phoneData.DDD);
    const phoneExists = await checkIfPhoneExists(randomPhoneNumber);
    if (phoneExists) {
        randomPhoneNumber = await generateRandomPhone(phoneData);
    }
    return randomPhoneNumber;
};

/**
 * Generates files with random phone number in 'phone' folder.
 * @param {PhoneData[]} phoneDataset
 */
const generateRandomPhonesFromDataset = async (phoneDataset) => {
    console.log(phoneDataset.length)
    for(let i = 0; i < phoneDataset.length; i++) {
        const phoneData = phoneDataset[i];
        const filesToGenerateNewPhone = phoneData.fileList.slice(1);
        for(let j = 0; j < filesToGenerateNewPhone.length; j++) {
            const file = filesToGenerateNewPhone[j];
            const randomPhoneNumber = await generateRandomPhone(phoneData);
            const resultsFromAPIs = await getAPIsData(phoneData.phoneNumber);
            const fileJSON = mountRandomPhoneNumberJSONFile(file, {
                originalPhoneNumber: phoneData.phoneNumber,
                newPhoneNumber: randomPhoneNumber,
            }, resultsFromAPIs)
            writeRandomPhoneFile(fileJSON);
            console.log(fileJSON);
        }
    }
};

module.exports = {
    getPhoneDatasetFromCherrinCherrionFiles,
    getRepeatedPhones,
    generateRandomPhonesFromDataset,
};

const request = require('../utils/request');
const config = require('../config');
const paths = config.SENTINEL_MANAGER_API.PATHS;

const getPathURL = (pathUrl, phoneNumber) => pathUrl + (phoneNumber ? `/${phoneNumber}` : "");

const getDataFromGetRequest = (url) => request.get(url)
    .then(({ data }) => data)
    .then(({ data }) => data);

const getPhoneAccumulatedBalances = (phoneNumber) =>
    getDataFromGetRequest(getPathURL(paths.ACCUMULATED_BALANCES, phoneNumber));

const getPhoneBalances = (phoneNumber) =>
    getDataFromGetRequest(getPathURL(paths.BALANCES, phoneNumber));

const getPhoneBuckets = (phoneNumber) =>
    getDataFromGetRequest(getPathURL(paths.BUCKETS, phoneNumber));

const getPhoneInvoices = (phoneNumber) =>
    getDataFromGetRequest(getPathURL(paths.INVOICES, phoneNumber));

const getPhonePayments = (phoneNumber) =>
    getDataFromGetRequest(getPathURL(paths.PAYMENTS, phoneNumber));

const getPhoneProducts = (phoneNumber) =>
    getDataFromGetRequest(getPathURL(paths.PRODUCTS, phoneNumber));

const postPhoneAccumulatedBalances = (phoneNumber, data) =>
    request.post(getPathURL(paths.ACCUMULATED_BALANCES), {
        data,
        phoneNumber,
    });

const postPhoneBalances = (phoneNumber, data) =>
    request.post(getPathURL(paths.BALANCES), {
        data,
        phoneNumber,
    });

const postPhoneBuckets = (phoneNumber, data) =>
    request.post(getPathURL(paths.BUCKETS), {
        data,
        phoneNumber,
    });

const postPhoneInvoices = (phoneNumber, data) =>
    request.post(getPathURL(paths.INVOICES), {
        data,
        phoneNumber,
    });

const postPhonePayments = (phoneNumber, data) =>
    request.post(getPathURL(paths.PAYMENTS), {
        data,
        phoneNumber,
    });

const postPhoneProducts = (phoneNumber, data) =>
    request.post(getPathURL(paths.PRODUCTS), {
        data,
        phoneNumber,
    });

const getAPIsData = (phoneNumber) => Promise.all([
    getPhoneAccumulatedBalances(phoneNumber),
    getPhoneBalances(phoneNumber),
    getPhoneBuckets(phoneNumber),
    getPhoneInvoices(phoneNumber),
    getPhonePayments(phoneNumber),
    getPhoneProducts(phoneNumber),
]);

const getPostMethod = (method) => {
    switch(method) {
        case config.SENTINEL_MANAGER_API.API_INDEX_NAME[0]:
            return postPhoneAccumulatedBalances;
        case config.SENTINEL_MANAGER_API.API_INDEX_NAME[1]:
            return postPhoneBalances;
        case config.SENTINEL_MANAGER_API.API_INDEX_NAME[2]:
            return postPhoneBuckets;
        case config.SENTINEL_MANAGER_API.API_INDEX_NAME[3]:
            return postPhoneInvoices;
        case config.SENTINEL_MANAGER_API.API_INDEX_NAME[4]:
            return postPhonePayments;
        case config.SENTINEL_MANAGER_API.API_INDEX_NAME[5]:
            return postPhoneProducts;
    }
} 

module.exports = {
    GET: {
        getPhoneAccumulatedBalances,
        getPhoneBalances,
        getPhoneBuckets,
        getPhoneInvoices,
        getPhonePayments,
        getPhoneProducts,
    },
    POST: {
        postPhoneAccumulatedBalances,
        postPhoneBalances,
        postPhoneBuckets,
        postPhoneInvoices,
        postPhonePayments,
        postPhoneProducts,
    },
    getAPIsData,
    getPostMethod,
};

const fs = require('fs');
const path = require('path');
const { promisify } = require('util');
const config = require('../config');

const access = promisify(fs.access);
const writeFile = promisify(fs.writeFile);
const readFile = filePath => fs.readFileSync(filePath, 'utf8');

/**
 * Get all files from folder reading sub-folders recursively.
 * @param {String} dir Directory to read
 * @param {String[]} [fileList=[]]
 * @returns {String[]}
 */
function getDirFilesList (dir, fileExtension = '.js', fileList = []) {
  const files = fs.readdirSync(dir);

  files.forEach((file) => {
      const stat = fs.statSync(path.join(dir, file));
      if (stat.isDirectory()) {
          fileList = getDirFilesList(path.join(dir, file), fileExtension, fileList);
      } else if (file !== __dirname && file.endsWith(fileExtension)) {
          fileList.push(path.join(dir, file));
      }
  })

  return fileList;
};

const cherrinCherrionDir = path.resolve(
  __dirname,
  '..',
  '..',
  'sentinel-dialog-cherrin-cherrion',
  '__tests__',
);

/**
 * Return all test files from cherrin-cherrion
 * @returns {String[]}
 */
const getCherrinCherrionFiles = () => getDirFilesList(cherrinCherrionDir);

const randomPhonesDir = path.resolve(__dirname, '..', 'random_phones');

const writeRandomPhoneFile = (data) => {
  writeFile(path.resolve(randomPhonesDir, `${data.newPhoneNumber}.json`), JSON.stringify(data, null, 2));
}

const getRandomPhoneFiles = () => getDirFilesList(randomPhonesDir, '.json');

const mountRandomPhoneNumberJSONFile = (file, { originalPhoneNumber, newPhoneNumber }, apisResult) => {
  const fileJSON = {
      file,
      originalPhoneNumber,
      newPhoneNumber,
      apisResult: {}
  };

  apisResult.forEach((result, index) => {
      const messageIsNotFound = result.message && result.message.startsWith("Can't was find this phoneNumber ");
      if (result.code === 404 || messageIsNotFound) return;
      fileJSON.apisResult[config.SENTINEL_MANAGER_API.API_INDEX_NAME[index]] = result;
  });

  return fileJSON;
}

const getDatasetFromRandomPhoneFiles = () => getRandomPhoneFiles()
    .map((file) => {
        const randomPhoneFileJSON = readFile(file);
        try {
            return JSON.parse(randomPhoneFileJSON); 
        } catch (err) {
            throw err;
        }
    });

module.exports = {
  access,
  writeFile,
  readFile,
  getDirFilesList,
  getCherrinCherrionFiles,
  getRandomPhoneFiles,
  writeRandomPhoneFile,
  mountRandomPhoneNumberJSONFile,
  getDatasetFromRandomPhoneFiles,
};

/**
 * Generates random phone number with a random DDD
 * example: 'rr99rrrrr' (r = random number)
 * @returns {String}
 */
const generateRandomPhoneNumberWithDDD = () => {
    const randomNumber = Math.floor(Math.random() * 10000000000000).toFixed();
    const randomDDD = randomNumber.slice(0, 2);
    const randomPhoneNumber = randomNumber.slice(2,9);
    return randomDDD.concat('99', randomPhoneNumber);
};

/**
 * Generates random phone number without addind a random ddd
 * example: '99rrrrr' (r = random number)
 * @returns {String}
 */
const generateRandomPhoneNumberWithoutDDD = () =>
    generateRandomPhoneNumberWithDDD().slice(2);

/**
 * Generates random phone number with adding an specific DDD
 * @param {String} ddd 
 * @returns {String}
 */
const generateRandomPhoneNumberWithSpecificDDD = (ddd) =>
    ''.concat(ddd, generateRandomPhoneNumberWithoutDDD());

module.exports = {
    generateRandomPhoneNumberWithDDD,
    generateRandomPhoneNumberWithoutDDD,
    generateRandomPhoneNumberWithSpecificDDD,
};

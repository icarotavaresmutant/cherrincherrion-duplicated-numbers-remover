const axios = require('axios');
const config = require('../config');

const requestInstance = axios.create({
    baseURL: config.SENTINEL_MANAGER_API.BASE_URL,
});

module.exports = requestInstance;
